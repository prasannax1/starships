# Starship Designs

Star Trek type starships

## Aviary Class

![Mother Ship](images/mothership.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/mother.stl)

Aviary Class Deep Space Explorer/Heavy Cruiser/Carrier

### Stats

 * Length: 1154m
 * Width: 580m
 * Height: 239m
 * Decks: 56
   * Saucer Section: 28 decks
   * Engine Section: 28 decks
 * Auxillary Craft - Large:
   * 14 *Magpie Class* Light Science vessels
   * 12 *Vulture Class* Light Corvette/Patrol vessels
   * 2 *White Owl Class* Light Scout vessels
   * 1 *Dove Class* Diplomatic Yacht/Light Courier
   * 1 *Nightingale Class* Light Hospital Ship
   * 1 Escort Vessel
 * Warp speed:
   * Cruising: Warp 5
   * Maximum: Warp 7.3
   * Transwarp: ??

### Description

The *Aviary Class* is meant to act as both a deep space explorer, and as a mobile starbase to launch other exploration missions from. It is designed primarily for scientific missions, but like any ship designed after Wolf 359 and The Dominion War, it can also be used in military conflicts.

### Structure

The *Aviary* follows a pretty tradtional design, saucer, secondary hull and nacelles. The saucer is shaped to present a lower profile from the fore direction. It is fatter than you expect it, even on a ship this size, because most of the lower half of the saucer section consists of mobile hangars for small starship.

#### Saucer Section

The saucer section is almost as big as a *Galaxy Class* by itself. It is split into 4 main parts.

The top 5 decks of the saucer section consists of the upper bridge module. It consists of main bridge, main computer core, an auxillary sickbay. It also contains offices for the civilian government and administration for the civilian and starfleet residents of the *Aviary*. It also contains a cafeteria and dormitory for bridge and admin staff.

The next 9 decks of the saucer section contains the main inhabitable area of the *Aviary*. It is split up into 16 sections and a central core. The central core consists of main power coupling, turbolift routing and an auxillary computer core.

Each section contains an auxillary sickbay, a brig, and a replimat. They contain residences for the crew for all the ships in the hangar underneath, as well as residences for everyone who works in that sections. 8 of the sections contain secondary computer cores.

In addition, each section also contains areas which specialize. 

 * Section 0 is the diplomatic office. It also contains residence for diplomatic guests, residences for admiralty, a food court and a holo conference auditorium.

 * Sections 1, 3, 5, 6, 10, 11, 13, 14 and 15 are various science labs. The labs are reconfigurable per ship. These sections also contain 2 holodecks each, and a medium food court.

 * Section 2 is the main hospital. It also contains a quarantine area.

 * Section 4 is auxillary engineering. It has engineering labs and a light industrial replicator.

 * Section 7 is internal security. It contains weapons locker, and barracks for security personnel all over the ship, and main brig complex.

 * Section 8 is main engineering. Section 8 is mainly concerned with the running of the massive warp and transwarp engines that the *Aviary* sports.

 * Section 9 is external security. Section 8 is responsible for the defense and tactical operations of *Aviary* as well as providing security for away missions.

 * Section 12 is auxillary guest quarters for civilian passengers. It also contains a large food court and a large holo-complex.

The next 10 decks of the saucer section contains 16 hangars, each measuring 150m long and 100m wide on the borad side, and 40m tall. In addition to the hangars, it also contains the central core which is the main cargo store.

Like the section above it, each hangar contains ships specialized to the offices they handle.

 * Section 0 contains diplomatic yachts (*Dove Class*).

 * Section 1 and 15 contain the advanced light scout *White Owl Class* vessels.

 * Section 2 contains the *Nightingale Class* light hospital ship and medevac runabouts.

 * Sections 3, 5, 6, 10, 11, 13 and 14 contain two light *Magpie Class* science vessels each.

 * Section 4 hangar is actually a repair shop. It contains a standard Federation Warp Tug, and empty space to take apart and repair any ship of the *Aviary* line-up.

 * Section 7 & Section 9 - internal security doesn't need ships, so both of these are used to store the 12 *Vulture Class* corvettes of the external security department.

 * Section 8 hangar is not a hangar at all - it contains the upper third of the massive warp core of the *Aviary* as well as main antimatter storage.

 * Section 12 is used to store security cleared and approved personal vehicles.

#### Captain's Yacht

In the space under the saucer traditionally used by Captain's yachts, the *Aviary* has a dedicated Escort vessel. Nicknamed the *Bat Class* (because it hangs upside down), is about as big as a *Defiant Class* and is as powerful as the *Defiant* would be if it was made today.

![Mother Ship Saucer](images/mothership-saucer-separation.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/ms_saucer.stl)

### "*Bat Class*" stats

 * Length: 245m
 * Width: 170m
 * Height: 21m
 * Decks: 5
 * Auxillary Craft:
   * 2 Medium Shuttles
 * Warp Speed:
   * Cruising: Warp 7
   * Maximum: Warp 9.5

#### Engineering section

The engineering section contains 26 decks. The front half is used to house the warp and transwarp cores of the ship. The aft section is used as storage. There is no shuttlebay since the *Aviary* has hangars to spare.

## Magpie Class

![Science Ship](images/science.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/science.stl)

Magpie Class Light Science Vessel

### Stats

 * Length: 69m
 * Width: 52m
 * Height: 17m
 * Decks: 2
 * Auxillary Craft: 1 Shuttlepod
 * Warp Speed:
   * Cruising: Warp 4
   * Maximum: Warp 8.5

### Description

The *Magpie Class* is the workhorse of the *Aviary* mini-fleet. Fast, agile and reasonable powerful, this ship is supposed to do the bulk of the exploring on any mission.

### Structure

The upper deck is essentially a very long runabout, with the cockpit in front followed by the main transporter, computer core and engineering. The warp core of the ship is laid out horizontally in the last third of the upper deck. In the middle, the upper deck also contains an elevator to the lower deck.

Approximately above the engine, the *Magpie* also contains a disc containing a sensor array, that can be reconfigured with mission specific sensors.

The lower deck contains up to 8 big modular lab rooms which can be removed and reconfigured as per missions. It also contains residential rooms for crew and scientists, a mess hall and a sickbay. Since the ship may or may not have a medical officer on a mission, each ship has an EMH manning the sickbay.

The aft end of the lower deck contains a small shuttlebay which houses a small shuttlepod for planetary landings.

## Vulture Class

![Attack Ship](images/attack.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/attack.stl)

Vulture Class Light Corvette/Patrol Vessel

### Stats

 * Length: 59m
 * Width: 31m
 * Height: 11m
 * Decks: 2.5 (2 + "attic" cargo deck)
 * Auxillary Craft: None
 * Warp Speed:
   * Cruising: Warp 7
   * Maximum: Warp 9.95

### Description

The *Vulture Class* is not just named after a bird of prey - for all practical purposes it is a Bird of Prey built to Star Fleet specifications.

The *Vulture Class* occupies a unique niche in starship where a runabout or a fighter craft just doesn't cut it, but a *Defiant Class* or equivalent isn't really readily available.  Typical deployment of these vessels is aboard a starbase, and typical missions involve escorting a larger vessel through troubled space. And coming back alone.

### Structure

Despite its compact appearance, the *Vulture* is built on a rather conservative pattern. It contains a saucer section and a secondary hull but they're both fused together. The Warp nacelles are attached directly to the saucer section instead of being attached by pylons.

The upper deck is essentially a runabout, with the cockpit in front followed by main transporter, dormitory, pantry, sickbay and gym. The aft part of the upper deck is a small cargo bay, which extends to an upper "attic" section above the main deck 1. This cargo section is directly connected to the torpedo launchers and can be used to hold extra torpedoes, or can be configured with a sensor array if required.

Deck 2 consists of engineering, with the deflector in front, the warp core and antimatter reserve extending almost the entire length of deck 2.

The saucer section is bigger than the interiors would indicate - and the extra space is occupied by a massive phaser array and auxillary antimatter reserve.

The warp core of the *Vulture* is much more powerful than a ship of this size would indicate, and even at it's top speed of Warp 9.95 it consumes only 25% of the power output. The overpowered warp core is mainly used to power the phaser array, which is what enables the *Vulture* to take out ships several times its size without breaking a sweat.

## White Owl Class

![Scout Ship](images/scout.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/scout.stl)

White Owl Class Light Scout

### Stats

 * Length: 113m
 * Width: 49m
 * Height: 21m
 * Decks: 5
   * Saucer Section: 3 decks
   * Engine Section: 2 decks
 * Auxillary Craft: 2 Shuttlepods
 * Warp Speed:
   * Cruising: Warp 7.5
   * Maximum: Warp 9.5
   
### Description

The *White Owl* is a bird of prey but it is also a curious bird. Accordingly this class of ships has better defences and offences than a *Magpie*, and better sensors than a *Vulture*. The *White Owl* is deployed as an advanced scout in places where trouble is expected but discoveries are hoped.

### Structure

This ship follows a standard federation structure - saucer section, secondary hull, and warp nacelles on pylons. 

The saucer section consists of 3 decks. 

 * Deck 1 - bridge. 
 * Deck 2 - crew quarters
 * Deck 3 - modular labs, cafeteria, sickbay and cargo bays.
 * Deck 4 - main engineering, shuttle bay
 * Deck 5 - main engineering, shuttle bay

The aft-most part of deck 2 & 3 contains the upper half of the warp core. Both of the engineering decks contain the lower half of the warp core in the foremost part, with the aft part of both decks joined to form the shuttlebay containing two shuttlepods.

Unlike the *Magpie* and *Vulture* classes which can be mass produced cheaply, the *White Owl* is much more expensive to manufacture. That's why the *Aviary* contains at most two of these.

## Dove Class

![Diplomatic Yacht](images/diplomatic.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/diplomat.stl)

Dove Class Diplomatic Yacht/Light Courier

### Stats

 * Length: 46m
 * Width: 27m
 * Height: 10m
 * Decks: 2
   * Saucer Section: 1 deck
   * Engine Section: 1 deck
 * Auxillary Craft: None
 * Warp Speed:
   * Cruising: Warp 5
   * Maximum: Warp 7.5

### Description

With the underside of the *Aviary* taken up by a dedicated escort vessel, that still leaves the niche of a diplomatic vessel unfulfilled. The *Dove* class, which was initially designed to transport VIPs in federation territory, fits into this niche naturally.

### Structure

Even though it resembles a starship with its structure of saucer - secondary hull - warp nacelle design, the *Dove* is actually a very large yacht.

The Saucer section contains the cockpit and main deflector at the front, followed by a circular corridor surrounding a pantry area to exit into the engineering room aft. The outer side of the corridor has 4 luxury rooms, two on either side.

The engineering section conatins the main warp core and transporters.

## Nightingale Class

![Hospital Ship](images/hospital.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/hospital.stl)

Nightingale Class Light Hospital Ship

### Stats

 * Length: 80m
 * Width: 42m
 * Height: 22m
 * Decks: 6
   * Saucer section: 1 decks
   * Engineering section: 5 decks
 * Auxillary Craft: None
 * Warp Speed:
   * Cruising: Warp 7
   * Maximum: Warp 9
   
### Description

On the *Aviary* an entire sector is dedicated to sickbays, hospitals and clinics for the health and wellness of the crew within. But quite often, Starfleet is asked to extend a hand of help to other species who do not have the medical facilities to deal with it.

Enter the *Nightingale*. Named after Florence Nightingale, it is as big as a general hospital itself, it is equipped with some highly specialized medical equipment.


### Structure

The *Nightingale* looks pregnant with it's oversized aft section. The main saucer contains the bridge module, with crew dormitory instead of captain's rooms. The engineering section extends seamlessly into what looks like an oversized cargo bay, but is actually 5 decks of mobile hospital.

  * Medical transporters with highly sensitive biofilters
  * 2 separate bio-shielded quarantine areas
  * The latest of scanners
  * Gallons of biomemetic gel
  * 6 EMH and 12 EMAH (emergency medical assistant hologram)
  * 1 ECH in case the captain of the ship (nominally, the CMO of the *Aviary*) is incapacitated on mission.
  * Mobile holo-emitter kits to send EMH on missions where humanoids cannot go on.

## Relative scale of ships

![scale](images/scale.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/scale.stl)

![sectors](images/sector.png)

[View 3d](https://github.com/prasannax1/openscad/blob/master/stl/combined.stl)
